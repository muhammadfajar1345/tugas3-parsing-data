import 'package:flutter/material.dart';
import 'get_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TUGAS3_Mobpro_Muhammad Fajar Wibisono',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'TUGAS3_Mobpro_Muhammad Fajar Wibisono'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  UserGet userGet;

  @override
  void initState() {
    super.initState();
    UserGet.connectToApiUser('3').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFC9E1F9
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 30),
                child: Text('WELCOME!',
                    style: TextStyle(
                      fontSize: 48.0,
                      color: Color(0xFF0563BA),
                      fontWeight: FontWeight.bold,
                    ))),
            Container(
              child: Column(
                children: [
                  Container(
                    width: 128.0,
                    height: 128.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(
                            (userGet != null) ? userGet.avatar : "Loading"),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    (userGet != null)
                        ? userGet.firstName + " " + userGet.lastName
                        : "Loading",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF0563BA),
                      fontWeight: FontWeight.bold,
                      fontSize: 24.0,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    (userGet != null) ? userGet.email : "Loading",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18.0,
                    ),
                  ),
                  SizedBox(
                    height: 100.0,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
