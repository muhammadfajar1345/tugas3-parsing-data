import 'package:http/http.dart' as http;
import 'dart:convert';

class UserGet {
  String id;
  String email;
  String firstName;
  String lastName;
  String avatar;

  UserGet({this.id, this.email, this.firstName, this.lastName, this.avatar});

  factory UserGet.createUserGet(Map<String, dynamic> object) {
    return UserGet(
        id: object['id'].toString(),
        email: object['email'],
        firstName: object['first_name'],
        lastName: object['last_name'],
        avatar: object['avatar']);
  }

  static Future<UserGet> connectToApiUser(String id) async {
    String apiURLPOST = 'https://reqres.in/api/users/' + id;
    var apiResult = await http.get(apiURLPOST);

    var jsonObject = json.decode(apiResult.body);

    // ignore: non_constant_identifier_names
    var UserData = (jsonObject as Map<String, dynamic>)['data'];

    // print(UserData);
    return UserGet.createUserGet(UserData);
  }
}
