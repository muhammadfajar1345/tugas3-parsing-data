## TUGAS 3 - PARSING DATA
![2](/uploads/987216ed218331b02c8b615dd9c7329a/2.PNG)



## Our Teams
**UTS MATAKULIAH MOBILE PROGRAMMING 2**
**-**
**TIF 18 CNS REGULER MALAM**
* [Muhammad Fajar Wibisono - 16111220](https://www.linkedin.com/in/fajarwibisono/)
* [Oktaviana Widyasari - 18111123]
* [Arnika Marbun - 18111289]



## SCREENCAPTURE & DOCUMENTATION
### Apps
![t_video6327700039305003412](/uploads/2943e4ae88586040373ae34c708b32c4/t_video6327700039305003412.mp4)

### Code [main Class]
![f2a2123c-276b-403b-a896-95f26af5e9e7](/uploads/9cd58c0105f4c076c2544d59d57ed318/f2a2123c-276b-403b-a896-95f26af5e9e7.webm)

### Code [get_model Class]
![1](/uploads/92f667d109ee1643045612d572e0d051/1.PNG)


[Flutter logo]: https://raw.githubusercontent.com/flutter/website/master/src/_assets/image/flutter-lockup.png
[flutter.dev]: https://flutter.dev
